# DockerPraxisphasen

# Praxisphase I
In diesem Git Repository findet Ihr alles, was ihr für die Praxisphase 1 braucht.

**Erste Schritte:**

Als Erstes testen wir, ob Docker korrekt installiert wurde. Gib dazu folgenden Befehl ein:

`docker run hello-world`

Dir sollte darauf hin folgendes ausgegeben werden:

```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
1b930d010525: Pull complete 
Digest: sha256:2557e3c07ed1e38f26e389462d03ed943586f744621577a99efb77324b0fe535
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.
[...]
```

Lies Dir die Ausgabe in Ruhe durch, denn sie bringt uns gleich zum nächsten Schritt:

**Einen Ubuntu Container starten:**

Einen Ubuntu Container startet mensch mit dem Befehl:

`docker run -it ubuntu /bin/bash`

Doch lies Dir die folgenden Zeilen erst noch durch, bevor du den Container startest!
Wir schauen uns den Befehl nun kurz genauer an. Dieser Befehl besteht aus verschiedenen Komponenten:

`<Programmname> <Optionen> <Image> <Befehl>`

In diesem Fall ist unser Programmname 'docker' und die Option ist 'run'.
Nach der Option 'run' geben wir noch Argumente an: '-it'. '-it' steht für 'interactive'. 
Diese Argumente sorgt dafür, dass wir später auch auf die Shell unseres Ubuntu Systems zugreifen 
können. 
Danach geben wir ein Image an, und übergeben dem Image den Befehl '/bin/bash'. Dadurch kommen wir in
 unserem Container gleich in die Bash Shell. 

Führe nun den Befehl aus und sieh was passiert. 

Wenn alles geklappt hat, sollte jetzt innerhalb einer Sekunde eine Ubuntu Maschine 
gestartet sein und du solltest Eingaben in dessen Bash Shell als root User tätigen können.

So schnell geht das!

Tobe dich in der Maschine ruhig ein bisschen aus. Du kannst nichts auf deinem Host System kaputt machen.
Solltest du deine Maschine absichtlich kaputt machen wollen, versuche doch mal den Befehl `rm -rf --no-preserve-root /* ` 
(löscht alle Dateien in Deinem Root Directory). Stelle davor aber sicher, 
dass Du Dich wirklich in deinem Ubuntu Container befindest. Du wirst feststellen, danach geht nichts mehr :) .

Du kannst den Container mit dem Befehl `exit` beenden. 

Als nächstes lernen wir den Befehl `docker ps -a` kennen. Dieser zeigt uns alle Container
an, die lokal existieren. Ohne das `-a` zeigt der Befehl nur die Container an, die gerade laufen. 

Du wirst festellen, dass aktuell keine Container gestartet sind. Das liegt unter anderem auch daran, dass Docker Container
schließt, wenn darauf kein aktiver Prozess mehr läuft. In Praxisphase III wird sich das ändern.

# Praxisphase II

**Jetzt geht es an das Dockerfile!**

Was ein Dockerfile ist hast du ja schon in der Theoriephase gelernt. Wir wollen in dieser Praxisphase einen Container bauen, der beim ausführen mit Cowsay eine Message ausgibt.
Erstelle mit `mkdir cowsay.docker` ein Directory in dem unser Dockerfile liegen soll. Mit dem Befehl `cd cowsay.docker` navigierst Du in das Verzeichnis. 
Nun erstellen wir mit Vim das Dockerfile. Gib ein: `vim Dockerfile`.

Jetzt befindest du dich im Vim Texteditor. Der Vim Texteditor ist ein bisschen eigen und
mensch braucht meist Zeit, um sich einzuarbeiten. Wenn das aber passiert ist, macht es großen Spaß mit ihm zu arbeiten.
Drücke `i` um in den insert mode zu kommen. Jetzt kannst du schreiben. 
Als erstes wollen wir definieren, welches Base Image wir benutzen wollen. In diesem Fall nehmen wir wieder ein Ubuntu Image.
Die erste Zeile muss dementsprechend ```FROM ubuntu``` lauten. 

Jetzt musst du kurz überlegen, welche Befehle wir nun brauchen. Wir wollen ein Programm installieren und zwar das Programm Cowsay. Wenn wir ein Programm installieren wollen,
macht es Sinn, das mit einem RUN Befehl zu machen. Dann wird das Programm beim builden einmal installiert und nicht jedes mal beim starten eines Containers. 
Auf Ubuntu können wir Programme mit der Anwendung 'apt' installieren. Bevor wir Cowsay installieren müssen wir jedoch unsere Paketquellen aktualisieren. 
Schreibe also in die nächste Zeile:`RUN apt update && apt install -y cowsay`.

Die -y Option sorgt in diesem Fall dafür, dass die Frage, ob das Programm installiert werden soll, mit 'yes' beantwortet 
werden soll. Beim builden kannst du keine Eingaben tätigen, also müssen wir das vorher festlegen.

So, jetzt ist unser Cowsay Packet installiert. Als nächstes wollen wir, dass Cowsay beim starten des Containers ausgeführt wird. 
Dafür nutzen wir den CMD Befehl. Schreibe: `CMD ./usr/games/cowsay "Hello World!"`

./PFAD führt in Ubuntu ein Programm aus. Der Pfad zu der ausführbaren Datei lautet in unserem Fall `/usr/games/cowsay`. Das "Hello World!" ist die Message, die wir Cowsay übergeben. Hier kannst du auch etwas anderes 
reinschreiben. 

Nun müssen wir unser Dockerfile noch speichern. Drücke `ESC` um aus dem 'insert mode' zu kommen. Gib danach `:wq` (write, quit) ein.
Jetzt müsste sich Vim beendet haben.


**Schaffe, Schaffe, Häusle baue! Let's Build!**

Nun müssen wir das Cowsay Image builden. Dazu nutzen wir den Befehl `docker build -t cowsay . `. 

Schauen wir uns den Befehl mal genauer an:`<Programmname> <Optionen> <PfadZumDockerfile>`.

Das `docker` ist unser Programmname. Die Optionen sind `build` und `-t`. Das `-t` steht für tag. Hier geben wir unserem Image einen Namen. In diesem Fall 'cowsay'. Als nächstes kommt der Pfad.
Diesen geben wir mit `.` an. Der `.` steht dafür, das der Pfad auf das Directory verweist, in dem du dich gerade befindest.
So, jetzt Feuer frei! Gib den Befehl ein und schieße ab!

`docker build -t cowsay .`

Der Output des Befehls sollte wie folgt aussehen:
```
Sending build context to Docker daemon  2.048kB
Step 1/3 : FROM ubuntu
 ---> 20bb25d32758
Step 2/3 : RUN apt update && apt install -y cowsay
 ---> Running in 065ac7805f80

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

Get:1 http://archive.ubuntu.com/ubuntu bionic InRelease [242 kB]
Get:2 http://security.ubuntu.com/ubuntu bionic-security InRelease [88.7 kB]
Get:3 http://archive.ubuntu.com/ubuntu bionic-updates InRelease [88.7 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic-backports InRelease [74.6 kB]
Get:5 http://security.ubuntu.com/ubuntu bionic-security/multiverse amd64 Packages [3451 B]
Get:6 http://security.ubuntu.com/ubuntu bionic-security/main amd64 Packages [317 kB]
Get:7 http://security.ubuntu.com/ubuntu bionic-security/universe amd64 Packages [143 kB]
Get:8 http://archive.ubuntu.com/ubuntu bionic/restricted amd64 Packages [13.5 kB]
Get:9 http://archive.ubuntu.com/ubuntu bionic/main amd64 Packages [1344 kB]
Get:10 http://archive.ubuntu.com/ubuntu bionic/multiverse amd64 Packages [186 kB]
Get:11 http://archive.ubuntu.com/ubuntu bionic/universe amd64 Packages [11.3 MB]
Get:12 http://archive.ubuntu.com/ubuntu bionic-updates/multiverse amd64 Packages [6955 B]
Get:13 http://archive.ubuntu.com/ubuntu bionic-updates/restricted amd64 Packages [10.7 kB]
Get:14 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 Packages [646 kB]
Get:15 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 Packages [913 kB]
Get:16 http://archive.ubuntu.com/ubuntu bionic-backports/universe amd64 Packages [3666 B]
Fetched 15.4 MB in 2s (7299 kB/s)
Reading package lists...
Building dependency tree...
Reading state information...
All packages are up to date.

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

Reading package lists...
Building dependency tree...
Reading state information...
The following additional packages will be installed:
  libgdbm-compat4 libgdbm5 libperl5.26 libtext-charwidth-perl netbase perl
  perl-modules-5.26
Suggested packages:
  filters cowsay-off gdbm-l10n perl-doc libterm-readline-gnu-perl
  | libterm-readline-perl-perl make
The following NEW packages will be installed:
  cowsay libgdbm-compat4 libgdbm5 libperl5.26 libtext-charwidth-perl netbase
  perl perl-modules-5.26
0 upgraded, 8 newly installed, 0 to remove and 0 not upgraded.
Need to get 6563 kB of archives.
After this operation, 41.7 MB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 perl-modules-5.26 all 5.26.1-6ubuntu0.3 [2763 kB]
Get:2 http://archive.ubuntu.com/ubuntu bionic/main amd64 libgdbm5 amd64 1.14.1-6 [26.0 kB]
Get:3 http://archive.ubuntu.com/ubuntu bionic/main amd64 libgdbm-compat4 amd64 1.14.1-6 [6084 B]
Get:4 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libperl5.26 amd64 5.26.1-6ubuntu0.3 [3527 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 perl amd64 5.26.1-6ubuntu0.3 [201 kB]
Get:6 http://archive.ubuntu.com/ubuntu bionic/main amd64 libtext-charwidth-perl amd64 0.04-7.1 [9492 B]
Get:7 http://archive.ubuntu.com/ubuntu bionic/main amd64 netbase all 5.4 [12.7 kB]
Get:8 http://archive.ubuntu.com/ubuntu bionic/universe amd64 cowsay all 3.03+dfsg2-4 [17.7 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 6563 kB in 0s (13.6 MB/s)
Selecting previously unselected package perl-modules-5.26.
(Reading database ... 4039 files and directories currently installed.)
Preparing to unpack .../0-perl-modules-5.26_5.26.1-6ubuntu0.3_all.deb ...
Unpacking perl-modules-5.26 (5.26.1-6ubuntu0.3) ...
Selecting previously unselected package libgdbm5:amd64.
Preparing to unpack .../1-libgdbm5_1.14.1-6_amd64.deb ...
Unpacking libgdbm5:amd64 (1.14.1-6) ...
Selecting previously unselected package libgdbm-compat4:amd64.
Preparing to unpack .../2-libgdbm-compat4_1.14.1-6_amd64.deb ...
Unpacking libgdbm-compat4:amd64 (1.14.1-6) ...
Selecting previously unselected package libperl5.26:amd64.
Preparing to unpack .../3-libperl5.26_5.26.1-6ubuntu0.3_amd64.deb ...
Unpacking libperl5.26:amd64 (5.26.1-6ubuntu0.3) ...
Selecting previously unselected package perl.
Preparing to unpack .../4-perl_5.26.1-6ubuntu0.3_amd64.deb ...
Unpacking perl (5.26.1-6ubuntu0.3) ...
Selecting previously unselected package libtext-charwidth-perl.
Preparing to unpack .../5-libtext-charwidth-perl_0.04-7.1_amd64.deb ...
Unpacking libtext-charwidth-perl (0.04-7.1) ...
Selecting previously unselected package netbase.
Preparing to unpack .../6-netbase_5.4_all.deb ...
Unpacking netbase (5.4) ...
Selecting previously unselected package cowsay.
Preparing to unpack .../7-cowsay_3.03+dfsg2-4_all.deb ...
Unpacking cowsay (3.03+dfsg2-4) ...
Setting up perl-modules-5.26 (5.26.1-6ubuntu0.3) ...
Setting up libgdbm5:amd64 (1.14.1-6) ...
Processing triggers for libc-bin (2.27-3ubuntu1) ...
Setting up libtext-charwidth-perl (0.04-7.1) ...
Setting up libgdbm-compat4:amd64 (1.14.1-6) ...
Setting up netbase (5.4) ...
Setting up libperl5.26:amd64 (5.26.1-6ubuntu0.3) ...
Setting up perl (5.26.1-6ubuntu0.3) ...
Setting up cowsay (3.03+dfsg2-4) ...
Processing triggers for libc-bin (2.27-3ubuntu1) ...
Removing intermediate container 065ac7805f80
 ---> 80cca55cd85a
Step 3/3 : CMD ./usr/games/cowsay "Hello World"
 ---> Running in 9bfc83e17a0b
Removing intermediate container 9bfc83e17a0b
 ---> 15ed2cff32f7
Successfully built 15ed2cff32f7
Successfully tagged cowsay:latest
```
In der Ausgabe kannst du sehen, wie Docker die Paketquellen aktualisiert und das Programm Cowsay (und alle dazu gehörigen Abhängigkeiten) installiert.

Mit dem Befehl `docker images` kannst du dir alle auf deinem Rechner vorhandenen images anzeigen lassen. Bei der Ausgabe des Befehls sollte das image cowsay erscheinen.


**What does the cow say?**

Starte nun den Container 'cowsay'. Wie das geht hast du ja schon in Praxisphase I gelernt.  Der 'cowsay' Container muss nur starten, seinen Zweck erfüllen und dann wieder geschlossen werden.
Deswegen reicht ein simpler run ohne weitere Optionen.

Falls du nicht weißt, wie Du den Container starten sollst, kannst Du mich gerne fragen.


# Praxisphase III


**Vorbereitungen**

Jetzt geht es ans Eingemachte. Damit wir hier auch was lernen, schreiben wir nun unseren Webserver Code in Python.
Dafür müssen wir erst mal unser cowsay.docker Directory verlassen. Gib dazu den Befehl `cd ..` ein.

Nun müssen wir ein neues Directory erstellen. Dies nennen wir `webserver.docker`. 
Schau am besten in der Praxisphase I nach, falls Du nicht mehr weißt, wie mensch das macht. 
Navigiere nun in dieses Directory ( `cd` Befehl ).


**Das Backend**

Jetzt erstellen wir ein Python file welches unseren Webserver darstellen wird. Dieses nennen wir `webserver.py`. Gib dazu `vim webserver.py` ein.

Den Python Code haben wir ja vorhin schon durchgesprochen. Achte bei Python auf die Einrückungen.
Wenn diese nicht richtig sind, wirst du einen Syntax Exception vom Interpreter bekommen.

Schreibe den Code nun von dem Projektor ab. Falls Du Fragen haben solltest: frage!
Wenn du mit dem Abschreiben fertig bist, speicherst Du wieder, indem du in vim zuerst mit `ESC` den Editmode verlässt und dan `:wq` eingibst.



**Das "Frontend"**

Soweit so gut, jetzt fehlt noch eine index.html Datei, die dein Server beim `GET /` Deines Webbrowsers ausgibt.

Diese index.html laden wir uns aus diesem Gitlab repository herunter. Das passiert mit dem Befehl: 

`wget https://gitlab.com/martin.wentzlaff/dockerpraxisphasen/raw/master/index.html`


Wenn du jetzt `ls` eingibst sollten sich in deinem webserver.docker Directory zwei Dateien (webserver.py und index.html) befinden. Jetzt fehlt nur noch ein Dockerfile.


**Hello Again! Das Dockerfile**

Erstelle mit Vim eine Dockerfile Datei. 
Als baseimage nutzen wir dieses mal ein python baseimage (ein debian:stretch mit python und python3 installiert). Unsere erste Zeile lautet also `FROM python`. 
Damit Python unseren Code ausführen kann, muss es natürlich eine Datei haben, aus der es den Code lesen kann. Dafür nutzen wir den COPY Befehl.
COPY kopiert die angegebene Datei aus dem directory, in dem dein Dockerfile liegt, beim builden des images. Die Syntax für COPY sieht wie folgt aus:

`COPY <PfadZurZuKopierendenDatei> <PfadAnDenKopiertWerdenSoll>`

In unserem Fall wollen wir die Dateien webserver.py und index.html direkt in das Root Directory
unseres Containers kopieren. Dafür brauchen wir zwei COPY Befehle. Das sieht dann so aus:

`COPY webserver.py /`

`COPY index.html /`

Als nächstes müssen wir den Port freigeben, unter dem unsere Webseite erreichbar sein soll.
Das machen wir mit dem EXPOSE Befehl. Schreibe in die nächste Zeile: `EXPOSE 8080`. Wir wählen den Port 8080 um Konflikten mit anderen Anwendungen, die auf Port 80 laufen könnten, aus dem Weg zu gehen. 

Als nächstes wollen wir, dass in unserem Container unsere webserver.py ausgeführt wird. Dafür nutzen wir wieder den `CMD` Befehl. Wie davor starten wir unsere webserver.py mit dem Programm `python3`.

Schreibe also: `CMD python3 webserver.py`

Und fertig ist unser Dockerfile. Das war es schon. 

Speicher wieder indem du zuerst `ESC` drückst und dann `:wq` eingibst.


**Build 'n Run**

Nun müssen wir nur noch unser image builden. Wir nennen unser image 'pythonwebserver'. Unser Befehl lautet also:

`docker build -t pythonwebserver .`

Wenn alles geklappt hat, können wir unseren selbstgebauten Webservercontainer starten. Dafür brauchen wir zwei neue Optionen:
`-d` und `-p`. Das `-d` steht für detached. Damit läuft unser Docker Container im Hintergrund weiter. Mit der -p Option mappen wir Ports. Das sieht dann wie folgt aus:
`-p PortAufDemHostSystem:PortDasImDockerfileExposedWurde`.
In unserem Fall können wir unsere Applikation auch auf den Port 8080 hören lassen. Danach musst du unserem Container nur noch einen Namen geben. 
Nennen wir ihn in diesem Fall `pythonwebservercontainer`. Unser ganzer Befehl zum starten lautet dann also:

`docker run -d -p 8080:8080 --name pythonwebservercontainer pythonwebserver`

Fühle dich frei mich zu fragen, solltest du den Befehl nicht verstehen. 

Wenn du den Befehl ausführst, sollte Dir ein kurzer Hash angezeigt werden. Mit `docker ps` kannst du überprüfen, ob der Container auch läuft. Wenn er läuft, kannst du bei Dir im Browser 
DEINEVMIP:8080 eingeben. Dir sollte dort eine 'Webseite' angezeigt werden :)

Herzlichen Glückwunsch. Du hast es geschafft: ein selbstgebauter Webserver, welcher Content ausliefert!

Wenn du deinen Webserver stoppen willst, kannst du das mit dem Befehl `docker stop CONTAINERNAME/CONTAINERID` tun - du entfernst ihn anschließend mit `docker rm CONTAINERNAME/CONTAINERID`.




**Transfer Aufgabe:**

Nun kannst du beweisen, was du jetzt gelernt hat. Baue einen Nginx Container auf Basis eines Ubuntu baseimages und lasse diesen die index.html von vorhin ausgeben. Die Nginx Applikation soll auf Port 80 hören.
Denke dabei an die Grundvoraussetzungen eines Webservers und an die Eigenheiten eines Dockercontainers. 

Solltest du nicht weiterkommen gibt es in dem directory 'hints' in diesem Git repository Hinweise, die dir helfen können. Du kannst mich gerne fragen, falls dr diese nicht weiterhelfen.


Ich hoffe, Du hattest Spaß. 

Deine freundliche Spinne aus der Nachbarschaft
